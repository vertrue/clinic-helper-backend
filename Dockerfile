# docker build -t clinic-helper-back .
FROM python:3.9-alpine
WORKDIR /app
COPY pip.txt .
RUN pip install -r pip.txt
ADD main.py ./
COPY api ./api
COPY mongo ./mongo
CMD ["python", "main.py"]