#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import (
    Flask,
    url_for,
    render_template,
    request
)
from flask_pydantic import validate

from .requests_models import *

import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

# mongo modules
from mongo import mongo_medicines as mongo_medicines



### METHODS ###

def test_medicines(method, body = None):
    if method == 'GET':
        mongo_medicines.add_test_medicines()
        return {'status': 'success'}
    else:
        return {'status': 'fail', 'message': 'use GET-method to access this API'}, 404

@validate()
def create_medicine(method, body: MedicineBodyModel, query: ClinicQueryModel):
    if method == 'POST':
            
        response = mongo_medicines.create_medicine(query.clinic_id, body)
        response['status'] = 'success'
        return response
    
    else:
        return {'status': 'fail', 'message': 'use POST-method to access this API'}, 404

@validate()
def get_medicines(method, query: ClinicQueryModel):
    if method == 'GET':

        response = {}
        response['medicines'] = mongo_medicines.get_medicines(query.clinic_id)
        response['total_count'] = len(response['medicines'])
        response['status'] = 'success'
        
        return response
    
    else:
        return {'status': 'fail', 'message': 'use GET-method to access this API'}, 404
