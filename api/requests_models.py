from typing import Optional
from pydantic import BaseModel

class ClinicQueryModel(BaseModel):
    clinic_id: str

class MedicineBodyModel(BaseModel):
    title: str
    count: Optional[float]
    price: Optional[float]
    unit_name: str
    medicine_id: Optional[str]
