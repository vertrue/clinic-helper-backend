#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import (
    Flask,
    url_for,
    render_template,
    request,
    abort
)
import json

from mongo import init_mongo

app = Flask(__name__)

### config ###
with open('config.json') as config_file:
    data = json.load(config_file)

mongo_url = data['mongo_url']
host = data['host']
port = data['port']

init_mongo.connect_db(mongo_url)
### end config ###


# medecines api router
from api import api_medicines

@app.route('/medicines/<path>', methods=['GET', 'POST'])
def medicines(path):
    
    if path == 'test':
        response = api_medicines.test_medicines(method = request.method)
    elif path == 'create':
        response = api_medicines.create_medicine(method = request.method, body = request.json, query = request.args.lists())
    elif path == 'get':
        response = api_medicines.get_medicines(method = request.method, query = request.args.to_dict())
    else:
        abort(404)
        
    return response
    

if __name__ == '__main__':
    app.run(host, port)
