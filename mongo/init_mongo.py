#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pymongo import MongoClient
import pymongo

__MONGO__ = None
def connect_db(url):
    CONNECTION_STRING = 'mongodb://' + url
    client = MongoClient(CONNECTION_STRING)
    global __MONGO__
    __MONGO__ = client['ClinicHelper']
