#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from pymongo import MongoClient
import pymongo
from mongo.init_mongo import __MONGO__

import uuid

# DEV add medicines
def add_test_medicines():
    collection = __MONGO__['medicines']
    
    clinic_id = 'a8678fd0-e160-4628-b5df-cca79a31b027'
    
    query = [
                {
                    'title': 'Тестові ліки',
                    'count': 12,
                    'price': 10.12,
                    'unit_name': 'амп',
                    'clinic_id': clinic_id,
                    'medecine_id': str(uuid.uuid4())
                },
                {
                    'title': 'Тестові ліки номер 2',
                    'count': 234,
                    'price': 734.20,
                    'unit_name': 'фл',
                    'clinic_id': clinic_id,
                    'medecine_id': str(uuid.uuid4())
                },
                {
                    'title': 'бінти',
                    'count': 100,
                    'price': 8.00,
                    'unit_name': 'шт',
                    'clinic_id': clinic_id,
                    'medecine_id': str(uuid.uuid4())
                }
            ]
    
    finded = collection.insert(query)
    
# create new medecine
def create_medicine(clinic_id, medicine):
    collection = __MONGO__['medicines']

    medicine = medicine.__dict__
    medicine['clinic_id'] = clinic_id
    medicine['medicine_id'] = str(uuid.uuid4())
    
    query = medicine
    
    finded = collection.insert_one(query)
    
    del medicine['_id']
    
    return medicine

# пet medicince by clinic_id
def get_medicines(clinic_id):
    collection = __MONGO__['medicines']
    
    query = {'clinic_id': clinic_id}
    
    return list(collection.find(query, {'_id': False}))
